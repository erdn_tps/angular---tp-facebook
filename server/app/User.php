<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    function posts() {
        return $this->hasMany( Post::class );
    }

    function comments() {
        return $this->hasMany( Comment::class );
    }

    function likes() {
        return $this->hasMany( Like::class );
    }

    function friends() {
        return $this->belongsTo( Friend::class );
    }

    function friend() {
        return $this->hasMany( Friend::class );
    }
}
