<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['content', 'user_id'];
    protected $hidden = ['likes'];

    function comments() {
        return $this->hasMany( Comment::class );
    }

    function user() {
        return $this->belongsTo( User::class );
    }

    function likes() {
        return $this->hasMany( Like::class );
    }
}
