<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return response()->json( $users )
            ->header( 'Access-Control-Allow-Origin', '*' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make( $input, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if( $validator->fails() ){
            return response()->json( $validator->errors() )
                ->header( 'Access-Control-Allow-Origin', '*' );
        }

        $user = User::create( $input );

        return response()->json( $user )
            ->header( 'Access-Control-Allow-Origin', '*' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if( is_null( $user ) ) {
            return response()->json( 'User not found' )
                ->header( 'Access-Control-Allow-Origin', '*' );
        }

        $user->posts;
        $user->friend;

        return response()->json( $user )
            ->header( 'Access-Control-Allow-Origin', '*' );

    }

    public function auth( Request $request ){
        $input = $request->all();

        $validator = Validator::make( $input, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if( $validator->fails() ){
            return response()->json( $validator->errors() )
                ->header( 'Access-Control-Allow-Origin', '*' );
        }

        $user = User::where( 'username', $input['username'] )
                    ->where( 'password', $input['password'] )
                    ->first();

        if( is_null( $user ) ){
            return response()->json( ['error' => 'Login failed'] )
                ->header( 'Access-Control-Allow-Origin', '*' );
        }

        return response()->json( $user )
                ->header( 'Access-Control-Allow-Origin', '*' );
    }

}
