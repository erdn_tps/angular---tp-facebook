<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Like;

class LikeController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $like = Like::create([
            "user_id" => $request->user_id,
            "post_id" => $request->post_id
        ]);

        return response()->json( $like )
            ->header( 'Access-Control-Allow-Origin', '*' );
    }

}
