<?php

namespace App\Http\Controllers;

use App\Friend;
use Illuminate\Http\Request;

class FriendController extends Controller
{
   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $friend = Friend::create([
            "user_id" => $request->user_id,
            "friend_id" => $request->friend_id
        ]);

        return response()->json( $friend )
            ->header( 'Access-Control-Allow-Origin', '*' );
    }


}
