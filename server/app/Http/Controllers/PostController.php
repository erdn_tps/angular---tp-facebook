<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Post;
use App\User;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response( Post::with('user', 'comments')->get())
            ->header( 'Access-Control-Allow-Origin', '*' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make( $input, [
            'content' => 'required'
        ]);

        if( $validator->fails() ){
            return response()->json( $validator->errors() )
                ->header( 'Access-Control-Allow-Origin', '*' );
        }

        $post = Post::create( $input, [
            "user_id" => $request->user_id
        ] );

        return response()->json( $post )
            ->header( 'Access-Control-Allow-Origin', '*' );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        if( is_null( $post ) ) {
            return response()->json( 'Post not found' )
                ->header( 'Access-Control-Allow-Origin', '*' );
        }

        $post->comments;
        $post->nb_likes = $post->likes->count();


        return response()->json( $post )
            ->header( 'Access-Control-Allow-Origin', '*' );
    }

    public function showForUser( User $user )
    {
        $posts = Post::with('comments')->where('user_id', $user->id)->get() ;
        return response()->json( $posts )
            ->header( 'Access-Control-Allow-Origin', '*' );
    }

}
