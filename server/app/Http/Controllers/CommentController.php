<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Comment;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make( $input, [
            'content' => 'required'
        ]);

        if( $validator->fails() ){
            return response()->json( $validator->errors() )
                ->header( 'Access-Control-Allow-Origin', '*' );
        }

        $comment = Comment::create( $input, [
            "user_id" => $request->user_id,
            "post_id" => $request->post_id
        ] );

        return response()->json( $comment )
            ->header( 'Access-Control-Allow-Origin', '*' );
    }
}
