<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $fillable = ['user_id', 'friend_id'];

    function user() {
        return $this->belongsTo( User::class );
    }

    function users() {
        return $this->hasMany( User::class );
    }
}
