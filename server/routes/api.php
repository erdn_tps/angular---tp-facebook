<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('users', 'UserController')
    ->except('create', 'edit', 'update', 'destroy');

Route::post( 'users/auth', 'UserController@auth' );

Route::resource('posts', 'PostController')
    ->except('create', 'edit', 'update', 'destroy');

Route::resource('comments', 'CommentController')
    ->except('create', 'show', 'edit', 'update', 'destroy');

Route::resource('likes', 'LikeController')
    ->except('create', 'edit', 'update', 'destroy');

Route::resource('friends', 'FriendController')
    ->except('create', 'edit', 'update', 'destroy');

Route::get('/posts/user/{user}', 'PostController@showForUser');
