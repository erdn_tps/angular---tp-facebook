<?php

use Faker\Generator as Faker;

$factory->define(App\Friend::class, function (Faker $faker) {
    return [
        'user_id' => App\User::all()->random()->id,
        'friend_id' => App\User::all()->random()->id
    ];
});
