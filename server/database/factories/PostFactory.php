<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'content' => $faker->sentence(20),
        'user_id' => App\User::all()->random()->id
    ];
});
