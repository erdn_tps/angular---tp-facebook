<?php

use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {
    return [
        'content' => $faker->sentence( 10 ),
        'user_id' => App\User::all()->random()->id,
        'post_id' => App\Post::all()->random()->id
    ];
});
