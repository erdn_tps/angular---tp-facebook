<?php

use Illuminate\Database\Seeder;
use App\Comment;
use App\Friend;
use App\Like;
use App\Post;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory( User::class, 35 )->create();
        factory( Post::class, 50 )->create();
        factory( Comment::class, 100 )->create();
        // factory( Like::class, 1500 )->create();
        // factory( Friend::class, 50 )->create();
    }
}
