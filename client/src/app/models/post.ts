import {Comment, CommentLiteral} from "./comment";

export class Post {
  public id: number;
  public content: string;
  public user_id: number;
  public created_at: string = '';

  public comments: Comment[] = [];

  constructor( postLiteral: PostLiteral ) {

    this.id = postLiteral.id;
    this.content = postLiteral.content;
    this.user_id = postLiteral.user_id;
    this.created_at = postLiteral.created_at;

    if( postLiteral.comments ) {
      for( let jsoncomment of postLiteral.comments ) {
        const comment = new Comment( jsoncomment );
        this.comments.push( comment );
      }
    }

  }

}

export interface PostLiteral {
  id: number;
  content: string;
  user_id: number;
  created_at: string;

  comments?: CommentLiteral[];
}
