export class Comment {
    public id: number;
    public content: string;
    public user_id: number;
    public post_id: number;
    public created_at: string = '';

    constructor( commentLiteral: CommentLiteral ) {

        this.id = commentLiteral.id;
        this.content = commentLiteral.content;
        this.user_id = commentLiteral.user_id;
        this.post_id = commentLiteral.user_id;
        this.created_at = commentLiteral.created_at;

    }

}

export interface CommentLiteral {
    id: number;
    content: string;
    user_id: number;
    post_id: number;
    created_at: string;
}
