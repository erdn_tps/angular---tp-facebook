import {PostLiteral} from "./post";

export class User {
  public id: number;
  public username: string = '';
  public password: string = '';
  public created_at: string = '';


  constructor( userliteral: UserLiteral) {

    this.id = userliteral.id;
    this.username = userliteral.username;
    this.password = userliteral.password;
  this.created_at = userliteral.created_at; 

  }

}

export interface UserLiteral {
  id: number;
  username: string;
  password: string;
  created_at: string;

  posts?: PostLiteral[];

}
