import { Component, OnInit } from '@angular/core';

import {User} from "../models/user";
import {Post} from "../models/post";

import {PostService} from "../services/post.service";

@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.scss']
})
export class WallComponent implements OnInit {

  public user: User;
  public posts: Post[] = [];

  public content: string = '';

  constructor( private postService: PostService ) { }

  ngOnInit() {
    this.postService
        .findForUser( 36 )
        .subscribe( posts => {
          this.posts = posts;
        }) ;
  }

  create() {
    this.postService
        .create( 36, this.content )
        .subscribe( post => {
          this.posts.push( post );
          this.content = '';
        });
  }

}
