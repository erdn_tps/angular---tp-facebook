import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { WallComponent } from './wall/wall.component';
import { UsersListComponent } from './users-list/users-list.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { NewsFeedComponent } from './news-feed/news-feed.component';
import { FriendComponent } from './friend/friend.component';
import { PostComponent } from './post/post.component';

@NgModule({
    declarations: [
        AppComponent,
        AuthComponent,
        LoginComponent,
        SignupComponent,
        WallComponent,
        UsersListComponent,
        HeaderComponent,
        MainComponent,
        NewsFeedComponent,
        FriendComponent,
        PostComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
