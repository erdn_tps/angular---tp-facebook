import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

import {Post} from "../models/post";
import {Comment} from "../models/comment";

import {PostService} from "../services/post.service";
import {CommentService} from "../services/comment.service";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  public post: Post;

  public content_input: string = '';


    constructor( private route: ActivatedRoute,
              private postService: PostService,
              private commentService: CommentService ) { }

  ngOnInit() {
    this.route.paramMap
        .subscribe( param => {
          const id = parseInt(param.get('id'));
          return this.postService
              .find( id )
              .subscribe( jsonpost => {
                this.post = new Post( jsonpost );
              });
        });

  }

    create() {
        this.commentService
            .create( this.content_input, 36, this.post.id )
            .subscribe( comment => {
                console.log(comment);
                this.post.comments.push( comment );
                this.content_input = '';
            });
    }

}
