import { Component } from '@angular/core';
import {Router} from "@angular/router";

import {User} from "../models/user";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {

  public error_username: boolean;
  public error_password: boolean;

  public username: string = '';
  public password: string = '';

  constructor( private userService: UserService,
               private router: Router) { }

  signup(): void {
    // on retire les espaces en début et fin de champ
    this.username = this.username.trim();
    this.password = this.password.trim();

    // on passe l'input username en rouge si le champ est vide
    if ( this.username === '') {
      this.error_username = true;
      return;
    }

    // on passe l'input password en rouge si le champ est vide
    if ( this.password === '') {
      this.error_password = true;
      return;
    }

    this.userService.create({ username: this.username, password: this.password } as User)
        .then( responseUser => {
          if(responseUser.success) this.router.navigateByUrl('/');
          else console.log( responseUser.error );
        });

  }

}
