import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { User } from '../models/user';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {

    public error_username: boolean;
    public error_password: boolean;

    public username: string = '';
    public password: string = '';

    constructor( private authService: AuthService,
                 private router: Router ) { }

    login(): void {
        // on retire les espaces en début et fin de champ
        this.username = this.username.trim();
        this.password = this.password.trim();

        // on passe l'input username en rouge si le champ est vide
        if ( this.username === '') {
            this.error_username = true;
            return;
        }

        // on passe l'input password en rouge si le champ est vide
        if ( this.password === '') {
            this.error_password = true;
            return;
        }

        //
        this.authService.auth({ username: this.username, password: this.password } as User)
            .then( responseUser => {
                if(responseUser.success) this.router.navigateByUrl('/');
                else console.log( responseUser.error );
            });

    }

}
