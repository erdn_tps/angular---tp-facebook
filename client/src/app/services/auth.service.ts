import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CanActivate, Router } from '@angular/router';

import {User} from "../models/user";

const serviceUrl: string = 'http://127.0.0.1:8000/api/users/auth';

export interface ResponseUser {
    success: boolean,
    user?: User,
    error?: string
}

@Injectable({
    providedIn: 'root'
})
export class AuthService implements CanActivate {

    public user: User = null;

    constructor( private http: HttpClient,
                 private router: Router ) { }

    public auth(user: User): Promise<ResponseUser> {

        const formData: FormData = new FormData();

        formData.append('username', user.username);
        formData.append('password', user.password);

        return fetch(serviceUrl, { method: 'POST', body: formData })
            .then( response => response.json() )
            .then( (user: User) => {
                if(user.id) {
                    this.user = user;
                    return { success: true, user: user };
                }
                else return { success: false, error: 'No user found' }
            } );

    }

    // on vérifie qu'il y a bien un user pour lui donner accès aux autres pages
    canActivate(): boolean {

        if ( this.user !== null ) {
            return true;
        }

        this.router.navigateByUrl( '/auth' );

        return false;
    }

}
