import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {Observable} from "rxjs";
import {map} from "rxjs/operators";

import {Post, PostLiteral} from "../models/post";

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private posts: Post[] = [];

  constructor( private http: HttpClient) { }

  findForUser( user_id: number ): Observable<Post[]> {

    const url = `http://127.0.0.1:8000/api/posts/user/${user_id}`;
    const obs =  this.http.get<PostLiteral[]>( url );

    return obs.pipe( map( (jsonposts) => {
    
      this.posts = [];

      for( let jsonpost of jsonposts ) {
        const post = new Post( jsonpost );
        this.posts.push( post );
      }

      return this.posts;

        }));
  }

  find( id: number ): Observable<PostLiteral> {
    return this.http.get<PostLiteral>( `http://127.0.0.1:8000/api/posts/${id}` );
  }

  create( user_id: number, content: string ): Observable<Post> {

    const url = `http://127.0.0.1:8000/api/posts`;
    const formData = new FormData;
    formData.append('user_id', user_id.toString() );
    formData.append('content', content);

    return this.http
        .post<PostLiteral>( url, formData )
        .pipe( map( jsonpost => new Post( jsonpost ) ) )

  }
}
