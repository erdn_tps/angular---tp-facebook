import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {Observable} from "rxjs";
import {map} from "rxjs/operators";

import {Comment, CommentLiteral} from "../models/comment";

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor( private http: HttpClient ) { }

  create( content: string, user_id: number, post_id: number ): Observable<Comment> {

    const url = `http://127.0.0.1:8000/api/comments`;
    const formData = new FormData;
    formData.append('content', content);
    formData.append('user_id', user_id.toString() );
    formData.append('post_id', post_id.toString() );

    return this.http
        .post<CommentLiteral>( url, formData )
        .pipe( map( jsoncomment => new Comment( jsoncomment ) ) )

  }
}
