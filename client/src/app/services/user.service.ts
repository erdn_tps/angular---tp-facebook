import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

import {Observable} from "rxjs";

import {User, UserLiteral} from "../models/user";
import {ResponseUser} from "./auth.service";
import {map} from "rxjs/operators";

const serviceUrl: string = 'http://127.0.0.1:8000/api/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

    public user: User = null;

    constructor( private http: HttpClient ) { }

    create( user: User ): Promise<ResponseUser> {
    const formData: FormData = new FormData();

    formData.append( 'username', user.username );
    formData.append( 'password', user.password );

    return fetch( serviceUrl, { method: 'POST', body: formData } )
        .then( response => response.json() )
        .then( (user: User) => {
          if(user.id) {
            this.user = user;
            return { success: true, user: user };
          }
          else return { success: false, error: 'No user created' }
        } );
    }

    all(): Observable<UserLiteral[]> {
        return this.http.get<UserLiteral[]>( serviceUrl );
    }

    add( id: number, friend: User ): Observable<boolean> {

        const url: string = `http://127.0.0.1:8000/api/friends`;

        return this.http
            .post<{ success: boolean }>( url, null )
            .pipe( map( data => data.success) );

    }
}

