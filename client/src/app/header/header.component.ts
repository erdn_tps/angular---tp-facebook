import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

import { User } from '../models/user';
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public user: User;

  constructor( private authService: AuthService,
               private router: Router ) { }

  ngOnInit() {
  }

  logout(): void {
    console.log('disconnect');
    // this.authService.user = null;
    // this.router.navigateByUrl( '/auth' );
  }

}
