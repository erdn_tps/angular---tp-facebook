import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../models/user";

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.scss']
})
export class FriendComponent implements OnInit {

  @Input() public input_friend: User;
  @Output() public friendEvent: EventEmitter<User> = new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  add() {
    this.friendEvent.emit( this.input_friend );
  }

}
