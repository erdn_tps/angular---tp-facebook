import { Component, OnInit } from '@angular/core';

import {UserService} from "../services/user.service";

import {User} from "../models/user";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  public users: User[] = [];
  public popup_text: string = '';
  public user_temp: User = null;


    constructor( private userService: UserService ) { }

  ngOnInit() {
    this.userService
        .all()
        .subscribe( jsonusers => {
          for( let jsonuser of jsonusers ) {
            const user = new User( jsonuser );
            this.users.push( user );
        }
        });
  }

  addFriend(user: User) {
      console.log(user.id);
      // this.user_temp = user;
      // this.userService
      //     .add( 36, user)
      //     .subscribe( success => {
      //
      //         if ( success )
      //             this.popup_text = `${this.user_temp.username} a été ajoutée à votre liste de personne à stalker.`;
      //         else
      //             this.popup_text = `${this.user_temp.username} est déjà dans votre liste de personne à stalker.`;
      //
      //         setTimeout(() => {
      //             this.popup_text = '';
      //         }, 2000);
      //     });
  }


}
