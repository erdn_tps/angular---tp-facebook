import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {AuthService} from './services/auth.service';

import { AuthComponent } from './auth/auth.component';
import { MainComponent } from "./main/main.component";
import { WallComponent } from "./wall/wall.component";
import { PostComponent } from "./post/post.component";
import { UsersListComponent } from "./users-list/users-list.component";
import { NewsFeedComponent } from "./news-feed/news-feed.component";

const routes: Routes = [
    { path: 'auth', component: AuthComponent },
];

const privateRoutes: Routes = [
    {
        path: '',
        component: MainComponent,
        // canActivate: [ AuthService ],
        children: [
            { path: '', redirectTo: 'wall', pathMatch: 'full' },
            { path: 'wall', component: WallComponent },
            { path: 'posts/:id', component: PostComponent },
            { path: 'users-list', component: UsersListComponent },
            { path: 'news-feed', component: NewsFeedComponent },
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot( routes ),
        RouterModule.forChild( privateRoutes ),
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }

